from django.contrib import admin
from django.urls import path
from . import views

#create your urls here
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.fitribookstore, name='fitribookstore'),
]