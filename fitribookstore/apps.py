from django.apps import AppConfig


class FitribookstoreConfig(AppConfig):
    name = 'fitribookstore'
