let button = $('#search-button')
let container = $('.table-of-books')

function ajax_func(value) {
    $('tbody').empty()
    let keywords = value
    console.log(keywords)
    $.ajax({
        method: 'GET',
        url: 'https://www.googleapis.com/books/v1/volumes?q=' + keywords,
        success: function(response) {
            for (let i = 0; i < response.items.length; i++) {
                let row_table = document.createElement('tr')
                let col_table = document.createElement('td')
                col_table.innerText = i + 1
                row_table.append(col_table)

                let img_cover = document.createElement('td')
                if (response.items[i].volumeInfo.imageLinks !== undefined) {
                    let image = document.createElement('img')
                    image.src = response.items[i].volumeInfo.imageLinks.thumbnail
                    img_cover.append(image)
                } else {
                    let image = document.createElement('img')
                    image.alt = "No Cover Available"
                    img_cover.append(image)
                }

                let book_title = document.createElement('td')
                book_title.innerText = response.items[i].volumeInfo.title

                let book_author = document.createElement('td')
                if (response.items[i].volumeInfo.authors !== undefined) {
                    for (let j = 0; j < response.items[i].volumeInfo.authors.length; j++) {
                        book_author.innerText = response.items[i].volumeInfo.authors[j]
                    }
                } else {
                    book_author.innerText = 'Author(s) Unknown'
                }

                row_table.append(img_cover, book_title, book_author)
                $('tbody').append(row_table)            
            }
        },
        error: function() {
            console.log('Failed')
        }
    })
}

$(document).ready(function() {
    ajax_func("world")

    $('#search-button').click(function(event) {
        $('.table_book').show()
        let keyword = $('#search-field').val()
        console.log(keyword)
        event.preventDefault()
        ajax_func(keyword)
    }) 
})
