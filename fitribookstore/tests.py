from django.test import TestCase,Client
from django.urls import reverse, resolve
from .views import fitribookstore

# Create your tests here.
class BookstoreTest(TestCase):
    def test_apakah_terdapat_url_fitribookstore(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_apakah_fungsi_fitribookstore_dijalankan(self):
        response = resolve(reverse('fitribookstore'))
        self.assertEqual(response.func, fitribookstore)

    def test_apakah_template_html_yang_digunakan_benar(self):
        response = Client().get('')
        self.assertTemplateUsed('fitribookstore.html')

    def test_apakah_terdapat_searchbox(self):
        response = Client().get('')
        content = response.content.decode('utf-8')
        self.assertIn('searchbox', content)

    def test_apakah_terdapat_table(self):
        response = Client().get('')
        content = response.content.decode('utf-8')
        self.assertIn('table-of-books', content)